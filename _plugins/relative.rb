# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
class Jekyll::Page

  def relative
    "../" * (url.count("/")-1)
  end

  def to_liquid(attrs = ATTRIBUTES_FOR_LIQUID)
    super(attrs + %w[
          relative
    ])

  end
end

class Jekyll::Post

  def relative
    "../" * (url.count("/")-1)
  end

  def to_liquid(attrs = ATTRIBUTES_FOR_LIQUID)
    super(attrs + %w[
          relative
    ])

  end
end
