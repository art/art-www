---
layout: default
title: Search Results
---
<!-- Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration -->

<section id="search-results" style="display: none;"> </section>

{% raw %}
<script id="search-results-template" type="text/mustache">
  {{#entries}}
    <article>
      <h4>
        {{#date}}<small><time datetime="{{pubdate}}" pubdate>{{displaydate}}</time></small>{{/date}}
        <a href=".{{url}}">{{title}}</a>
      </h4>
      {{#is_post}}
      <ul>
        {{#tags}}<li>{{.}} </li>{{/tags}}
      </ul>
      {{/is_post}}
    </article>
  {{/entries}}
</script>
{% endraw %}

<script type="text/javascript">
  $(function() {
    $('#search-query').lunrSearch({
      indexUrl  : './js/index.json',           // url for the .json file containing search index data
      results   : '#search-results',          // selector for containing search results element
      template  : '#search-results-template', // selector for Mustache.js template
      titleMsg  : '<h3>Search Results<h3>',   // message attached in front of results (can be empty)
      emptyMsg  : '<h3>Nothing found.</h3>'     // shown message if search returns no results
    });
  });
</script>
