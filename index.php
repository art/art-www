---
layout: default
title: Home
---
<!-- Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration -->
<div class="row">
  <div class="col-md-12">
    <div class="well well-sm">
      <h2>The ATLAS ART Project</h2>
      <p>ART - ATLAS Release Tester</p>
      <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#details" aria-expanded="false" aria-controls="details">
        Details...
      </button>
    </div>

    <div class="collapse" id="details">
      <div class="well"
        <p>The ATLAS Release Tester ...
        <p>
      </div>
    </div>
  </div>
</div>
