<!-- Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration -->
# ART Website

This is the ART project top-level website. Its contains MarkDown (.md) and ascidoc (.adoc) files.
On commit and push, the manuals (.adoc) are converted to html and pdf, then the site is converted by Jekyll
and published to the staging area.
On tag and push, the site including manuals is published to the production area.
