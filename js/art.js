// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Fix for NOSPAM email addresses, mark with class="mail"
//
jQuery(document).ready(function() {
  "use strict";
  $('a.mail').on('click', function() {
      var href = $(this).attr('href');
      $(this).attr('href', href.replace('@NOSPAM.', '@'));
  });
});
